# DESCRIPTION #

This is our first practical SDL2 example, building on the previous example where we opened a window.  

Here we load an image in a very simple format, the convert this to a format to be passed to the graphics card. Finally we draw this to screen.  This example is limited in that it uses SDLs native image loading, which is limited to bitmaps.  This is a problem we'll solve in a later example.

This code sample:

1.  Opens a window
2.  Sets up for hardware rendering
2.  Loads a bmp image
3.  Creates a copy to be passed to the graphics card
3.  Renders this to the screen

A sample image is provided.

|Version|Status|
|:--|:-:|
|Linux|**DONE**|
|Windows|**DONE**| 

# INSTRUCTIONS #

At present I'm maintaining a version for Command Line Linux and Visual Studio 2019 only, if other versions are required let me know and I'll set them up.

The build environment on Windows takes a bit of setting up if you want to do it all yourself, however, I will provide a video detailing the setup process for anyone who wants to setup their project from scratch. 

## Visual Studio 2019 ##

1. Open the project
2. Build
3. Debug
   
There should be no additional steps, a post-build process will copy the required dll files to the output directory and the debugger is set to run from there.

## Linux ##

Make sure you have the these packages installed:
```
sdl2
sdl2-devel
sdl2_image
sdl2_image-devel
```

Package names may vary on other flavours of Linux (these are the Fedora ones). 

1. Open a terminal in the Linux folder (or via MS Code)
2. Compile ```g++ -o test -lSDL2 -lSDL2_image main.cpp```
3. Run ```./test```

Debugging via gdb is possible but painful, consider using an editor if required. 
